Source: python-django-jsonfield
Section: python
Priority: optional
Maintainer: Debian Python Modules Team <python-modules-team@lists.alioth.debian.org>
Uploaders: Raphaël Hertzog <hertzog@debian.org>, Brian May <bam@debian.org>
Standards-Version: 3.9.8
Build-Depends:
 debhelper (>= 9),
 dh-python,
 python-all,
 python-django,
 python-setuptools,
 python-six,
 python3-all,
 python3-django,
 python3-setuptools,
 python3-six
Vcs-Git: https://anonscm.debian.org/git/python-modules/packages/python-django-jsonfield.git
Vcs-Browser: https://anonscm.debian.org/cgit/python-modules/packages/python-django-jsonfield.git
Homepage: https://bitbucket.org/schinckel/django-jsonfield/

Package: python-django-jsonfield
Architecture: all
Depends: python-django (>= 1.3), ${misc:Depends}, ${python:Depends}
Description: JSON field for Django models (Python 2)
 This package provides a Django field (jsonfield.JSONField) that you can use to
 store arbitrary JSON structures in a simple text field at the database level.
 .
 Accessing the field returns a decoded object (list, dict, string).
 .
 In forms, it appears like a TextField but the input is validated to be valid
 JSON.
 .
 This is the Python 2 version of the package.

Package: python3-django-jsonfield
Architecture: all
Depends: python3-django (>= 1.3), ${misc:Depends}, ${python3:Depends}
Description: JSON field for Django models (Python 3)
 This package provides a Django field (jsonfield.JSONField) that you can use to
 store arbitrary JSON structures in a simple text field at the database level.
 .
 Accessing the field returns a decoded object (list, dict, string).
 .
 In forms, it appears like a TextField but the input is validated to be valid
 JSON.
 .
 This is the Python 3 version of the package.
